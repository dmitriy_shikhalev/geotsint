"""geotsint URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import os
from django.conf.urls import url
from django.contrib import admin
from main import views
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from . import settings

urlpatterns = [
    url(r'^login/', views.LoginView.as_view()),
    url(r'^logout/', views.LogoutView.as_view()),
    url(r'^register/', views.RegisterView.as_view()),
    url(r'^activate/(?P<id>\d+)/(?P<code>\w+)/', views.ActivateView.as_view()),
    
    url(r'^$', login_required(login_url='/login/')(views.MainView.as_view())),
    url(r'^select_product/', login_required(login_url='/login/')(views.SelectProductView.as_view())),
    url(r'^create_product/', login_required(login_url='/login/')(views.CreateProductView.as_view())),
    url(r'^create_futures/(?P<product_id>\d+)/', login_required(login_url='/login/')(views.CreateFuturesView.as_view())),
    url(r'^futures_view/(?P<id>\d+)/', login_required(login_url='/login/')(views.FuturesView.as_view())),
    url(r'^futures_view/', login_required(login_url='/login/')(views.FuturesListView.as_view())),
    url(r'^null/(?P<id>\d+)/', login_required(login_url='/login/')(views.NullView.as_view())),
    #url(r'^select_mirror/(?P<id>\d+)/(?P<mirror_id>\d+)/', login_required(login_url='/login/')(views.FuturesSelectMirrorView.as_view())),
    # url(r'^deal_view/(?P<id>\d+)/', login_required(login_url='/login/')(views.DealView.as_view())),
    # url(r'^submit/(?P<id>\d+)/', login_required(login_url='/login')(views.SubmitDealView.as_view())),
    url(r'^ok/(?P<id>\d+)/', login_required(login_url='/login')(views.OkDealView.as_view())),
    url(r'^cancel/(?P<id>\d+)/', login_required(login_url='/login')(views.CancelDealView.as_view())),
    url(r'^deal_view/', login_required(login_url='/login/')(views.DealListView.as_view())),
    url(r'^edit_contacts/', login_required(login_url='/login/')(views.EditContactsView.as_view())),
    url(r'^error/', views.ErrorView.as_view()),
    url(r'^about/', views.AboutView.as_view()),
    url(r'^current/', views.CurrentProductList.as_view()),
    
    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
"""
urlpatterns = []
"""
