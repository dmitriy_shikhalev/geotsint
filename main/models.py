from django.db import models
from django.contrib import auth
import string
import random
import decimal
import datetime


FUTURES_STATUSES = (
    ('free', 'free'),
    ('busy', 'busy'),
    ('done', 'done'),
    ('canc', 'canc'),
)
DEAL_STATUSES = (
    # ('void', 'void'), # Annullirovan
    ('wait', 'wait'),
    # ('busy', 'busy'),
    ('done', 'done'),
    ('canc', 'canc'),
)
ROUTES = (
    ('buy', 'покупка'),
    ('sell', 'продажа'),
)
USER_STATUSES = (
    ('', ''),
    ('created', 'created'),
    ('confirm', 'confirm'),
)

PRICE_MAX_DIGITS = 10
REPUTATION_MAX_DIGITS = 4
USER_STATUS_MAX_LENGTH = 7
EMAIL_CONFIRMATION_MAX_LENGTH = 20
REPUTATION_DIGITS = 5
WEEKDAYS = (
    'mon',
    'tue',
    'wed',
    'thu',
    'fri',
    'sat',
    'sun',
)

MIN_DEALS_AMOUNT = 10
MIRROR_LIMIT = 20


class User(models.Model):
    user = models.OneToOneField(auth.models.User)
    contacts = models.TextField()
    email_confirmation = models.CharField(max_length=EMAIL_CONFIRMATION_MAX_LENGTH)
    reputation = models.DecimalField(
        max_digits = REPUTATION_DIGITS+1,
        decimal_places = REPUTATION_DIGITS,
        default = '0.0',
    )
    status = models.CharField(
        default = '',
        max_length = USER_STATUS_MAX_LENGTH,
        choices = USER_STATUSES,
        null = True,
        blank = True
    )
    
    def gen_email_confirmation(self):
        choices = string.ascii_letters
        
        result = ''
        for i in range(EMAIL_CONFIRMATION_MAX_LENGTH):
            char = random.choice(choices)
            result += char
        
        self.email_confirmation = result
    
    def calculate_reputation(self):
        deals = Deal.objects.filter(
            models.Q(buyer_futures__goods__user=self)
            | models.Q(seller_futures__goods__user=self)
        ).filter(status__in=('done', 'canc'))
        if deals.count() > MIN_DEALS_AMOUNT:
            self.reputation = decimal.Decimal(
                deals.filter(status='done').count() / deals.count()
            )
        else:
            pass
    
    def save(self, *args, **kwargs):
        if not self.status and not self.email_confirmation:
            self.gen_email_confirmation()
        return super(User, self).save(*args, **kwargs)
    
    def __str__(self):
        return '{} {} {}'.format(self.user, self.reputation, self.status)

class LoginIP(models.Model):
    user = models.ForeignKey(User)
    ip = models.GenericIPAddressField()

class Product(models.Model):
    relevant = models.PositiveIntegerField(default=0) # For choose products
    name = models.CharField(max_length=1024)
    words = models.ManyToManyField('Word', blank=True) # Fill with creation
    property_name_list = models.ManyToManyField('PropertyName', blank=True)
    
    def save(self, *args, **kwargs):
        ret = super().save(*args, **kwargs)
        
        chunks = self.name.split()
        for chunk in chunks:
            if len(chunk) >= Word.MIN_LENGTH:
                word = Word.objects.get_or_create(val=chunk.lower())[0]
                self.words.add(word)
        
        return ret
    
    def __str__(self):
        return self.name

class Word(models.Model):
    val = models.CharField(max_length=50, unique=True)
    
    MIN_LENGTH = 3
    
    def __str__(self):
        return self.val

class PropertyName(models.Model):
    #id = models.AutoField()
    name = models.CharField(max_length=50)
    
    @classmethod
    def name_list(cls):
        lst = cls.objects.order_by('name').values('name')
        rst = [x['name'] for x in lst]
        return rst
    
    def __str__(self):
        return self.name

class Goods(models.Model):
    user = models.ForeignKey('User')
    product = models.ForeignKey('Product')
    property_list = models.ManyToManyField('Property')
    photo = models.URLField(null=True, blank=True)
    
    def __str__(self):
        return '{}'.format(self.product.name)

class Property(models.Model):
    name = models.ForeignKey('PropertyName')
    value = models.CharField(max_length=50)
    
    def __str__(self):
        return '{}: {}'.format(self.name.name, self.value)

class Futures(models.Model):
    goods = models.ForeignKey('Goods')
    price = models.DecimalField(max_digits=PRICE_MAX_DIGITS, decimal_places=0) # No decimal 
    route = models.CharField(max_length=4, choices=ROUTES)
    date_from = models.DateField()
    date_till = models.DateField()
    # Weekdays
    mon = models.BooleanField()
    tue = models.BooleanField()
    wed = models.BooleanField()
    thu = models.BooleanField()
    fri = models.BooleanField()
    sat = models.BooleanField()
    sun = models.BooleanField()
    # ...
    time_from = models.TimeField()
    time_till = models.TimeField()
    address = models.CharField(max_length=200)
    status = models.CharField(max_length=4, choices=FUTURES_STATUSES, default='free')
    
    @property
    def user(self):
        return self.goods.user
    
    @property
    def reputation(self):
        return self.user.reputation
    
    @property
    def weekdays(self):
        if hasattr(self, '_mirror_futures'):
            return '{}{}{}{}{}{}{}'.format(
                int(self.mon) and int(self._mirror_futures.mon),
                int(self.tue) and int(self._mirror_futures.tue),
                int(self.wed) and int(self._mirror_futures.wed),
                int(self.thu) and int(self._mirror_futures.thu),
                int(self.fri) and int(self._mirror_futures.fri),
                int(self.sat) and int(self._mirror_futures.sat),
                int(self.sun) and int(self._mirror_futures.sun),
            )
        else:
            return '{}{}{}{}{}{}{}'.format(
                int(self.mon),
                int(self.tue),
                int(self.wed),
                int(self.thu),
                int(self.fri),
                int(self.sat),
                int(self.sun),
            )
    
    def get_route(self):
        for en, ru in ROUTES:
            if en == self.route:
                return ru
    
    def mirror_futures(self):
        route = 'buy' if self.route == 'sell' else 'sell'
        q = models.Q(address=self.address) | models.Q(address__startswith=self.address)
        res = ''
        for st in self.address.split(', '):
            if res:
                res += ', '.join((res, st))
            else:
                res = st
            q = q | models.Q(address=res)
        queryset = Futures.objects.filter(
            models.Q(
                # Address
                q
                
                # Date
                & ~models.Q(
                    models.Q(
                        date_till__lt = self.date_from
                    )
                    | models.Q(
                        date_from__gt = self.date_till
                    )
                )
                
                # Weekdays
                & models.Q(
                    models.Q(
                        models.Q(
                            mon = self.mon
                        )
                        & models.Q(
                            mon = True
                        )
                    )
                    | models.Q(
                        models.Q(
                            tue = self.tue
                        )
                        & models.Q(
                            tue = True
                        )
                    )
                    | models.Q(
                        models.Q(
                            wed = self.wed
                        )
                        & models.Q(
                            wed = True
                        )
                    )
                    | models.Q(
                        models.Q(
                            thu = self.thu
                        )
                        & models.Q(
                            thu = True
                        )
                    )
                    | models.Q(
                        models.Q(
                            fri = self.fri
                        )
                        & models.Q(
                            fri = True
                        )
                    )
                    | models.Q(
                        models.Q(
                            sat = self.sat
                        )
                        & models.Q(
                            sat = True
                        )
                    )
                    | models.Q(
                        models.Q(
                            sun = self.sun
                        )
                        & models.Q(
                            sun = True
                        )
                    )
                )
                
                # Time
                & models.Q(
                    models.Q( # time_from in (time_from, time_till)
                        models.Q(time_from__gte=self.time_from)
                        & models.Q(time_from__lte=self.time_till)
                    )
                    | models.Q( # time_till in (fime_from, time_till)
                        models.Q(time_till__gte=self.time_from)
                        & models.Q(time_till__lte=self.time_till)
                    )
                    | models.Q( # time_from, time_till contains (time_from, time_till)
                        models.Q(time_from__lt=self.time_from)
                        & models.Q(time_till__gt=self.time_till)
                    )
                ),
            ),
            route = route,
            goods__product = self.goods.product,
            status = 'free',
            goods__user__reputation__lte = self.reputation,
        ).exclude(goods__user__id = self.goods.user_id)
        
        queryset = queryset.order_by('-goods__user__reputation',  'price')
        queryset = queryset[:MIRROR_LIMIT]
        L = []
        for q in queryset:
            q._mirror_futures = self
            L.append(q)
        
        return L
    
    def calc_date_from(self):
        return max(self.date_from, self._mirror_futures.date_from)
    
    def calc_date_till(self):
        return min(self.date_till, self._mirror_futures.date_till)
    
    def calc_time_from(self):
        return max(self.time_from, self._mirror_futures.time_from)
    
    def calc_time_till(self):
        return min(self.time_till, self._mirror_futures.time_till)
    
    def is_actural(self):
        if self.date_till >= datetime.date.today():
            return True
        return False

class Deal(models.Model):
    buyer_futures = models.ForeignKey(Futures, related_name='buyer_deal')
    seller_futures = models.ForeignKey(Futures, related_name='seller_deal')
    date_from = models.DateField()
    date_till = models.DateField()
    # Weekdays
    mon = models.BooleanField()
    tue = models.BooleanField()
    wed = models.BooleanField()
    thu = models.BooleanField()
    fri = models.BooleanField()
    sat = models.BooleanField()
    sun = models.BooleanField()
    # ...
    time_from = models.TimeField()
    time_till = models.TimeField()
    price = models.DecimalField(max_digits=PRICE_MAX_DIGITS, decimal_places=0)
    address = models.CharField(max_length=200)
    status = models.CharField(max_length=4, choices=DEAL_STATUSES)
    
    @property
    def weekdays(self):
        return '{}{}{}{}{}{}{}'.format(
            int(self.mon),
            int(self.tue),
            int(self.wed),
            int(self.thu),
            int(self.fri),
            int(self.sat),
            int(self.sun),
        )
    
    def save(self, *args, **kwargs):
        res = super().save(*args, **kwargs)
        if self.status in ('done', 'canc'):
            u1 = self.buyer_futures.goods.user
            u2 = self.seller_futures.goods.user
            for u in (u1, u2):
                u.calculate_reputation()
                u.save()
        
        return res
    
    def get_self_futures(self, user):
        if self.buyer_futures.goods.user.id == user.id:
            return self.seller_futures
        else:
            return self.buyer_futures
    
    @property
    def partner(self):
        self_user = self._self_user
        if self.buyer_futures.goods.user.id == self_user.id:
            return self.seller_futures.goods.user
        else:
            return self.buyer_futures.goods.user
