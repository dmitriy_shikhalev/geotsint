from django.views.generic import TemplateView
from django.views.generic import RedirectView
from django.views.generic import View
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.views.generic.edit import FormView
from django.contrib import auth
from django.core.mail import send_mail
from django.db.models import Q

import datetime

import django.forms
from . import forms, models


# Login/register
class LoginView(FormView):
    template_name = 'login.html'
    form_class = forms.LoginForm
    success_url = '/'
    
    def form_valid(self, form):
        if self.request.user.is_authenticated():
            logout(self.request)
        login(self.request, form.user)
        
        # Get & save IP
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')
        models.LoginIP.objects.create(user=form.user.user, ip = ip)
        
        return super(LoginView, self).form_valid(form)

# Logout
class LogoutView(RedirectView):
    redirect_url = '/login/'
    
    def get(self, request):
        if request.user.is_authenticated():
            logout(request)
        
        return redirect(self.redirect_url)

# Register
class RegisterView(FormView):
    template_name = 'register.html'
    form_class = forms.RegisterForm
    success_url = '/login'
    
    def form_valid(self, form):
        # Create user
        user = auth.models.User.objects.create(
            username = form.cleaned_data['login'],
            email = form.cleaned_data['email'],
        )
        user.set_password(form.cleaned_data['password'])
        user.save()
        
        # Create main user
        main_user = models.User.objects.create(user=user, contacts=form.cleaned_data['contacts'])
        
        # Send e-mail
        send_mail(
            'Подтверждение регистрации',
            'Перейдите по ссылке для подтверждения регистрации: http://geotsint.ru/activate/{}/{}\nВаш логин: {}, пароль: {}'.format(
                main_user.id,
                main_user.email_confirmation,
                user.username,
                form.cleaned_data['password'],
            ),
            'noreply@geotsint.ru',
            [user.email],
        fail_silently=False)
        
        return super(RegisterView, self).form_valid(form)

# Activate account
class ActivateView(View):
    def get(self, request, id, code):
        user = models.User.objects.get(id=id)
        if user.email_confirmation == code:
            user.status = 'created'
            user.save()
        return redirect('/')

# Main page
class MainView(TemplateView):
    template_name = 'main.html'

# Select product
class SelectProductView(TemplateView):
    template_name = 'select_product.html'
    form_class = forms.SelectProductForm
    
    def get_context_data(self, **kwargs):
        context = super(SelectProductView, self).get_context_data(**kwargs)
        # Init form
        init = dict(words='')
        if self.request.GET.get('words'):
            init['words'] = self.request.GET['words']
        context['form'] = self.form_class(init)
        
        if not init['words']:
            context['products'] = models.Product.objects.all().order_by('-relevant')[:20]
        else:
            words = [word.lower() for word in init['words'].split()]
            
            words_inst_queryset = models.Word.objects.filter(val__in=words)
            products = models.Product.objects.filter(
                words__val__in = words
            ).order_by('-relevant')
            context['products'] = products
        
        return context

# Create product
class CreateProductView(TemplateView):
    template_name = 'create_product.html'
    form_class = forms.CreateProductForm
    redirect_url = '/create_futures/{}/'
    
    def get_context_data(self, **kwargs):
        context = super(CreateProductView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        return context
    
    def post(self, request):
        form = dict()
        form['name'] = self.request.POST['name']
        form['property_name_list'] = self.request.POST.getlist('property_name_list')
        
        product = models.Product.objects.create(name=form['name'])
        for property_name in form['property_name_list']:
            product.property_name_list.add(
                models.PropertyName.objects.get_or_create(
                    name = property_name
                )[0]
            )
        
        return redirect(self.redirect_url.format(product.id))

# Create futures
class CreateFuturesView(FormView):
    template_name = 'create_futures.html'
    form_class = forms.CreateFuturesForm
    redirect_url = '/futures_view/{}/'
    
    def get_form(self, *args, **kwargs):
        product = models.Product.objects.get(id=self.request.resolver_match.kwargs['product_id'])
        
        property_name_list = []
        for property_name in product.property_name_list.all():
            property_name_list.append(
                (str(property_name.id), property_name.name)
            )
        
        form = self.form_class(property_name_list = property_name_list)
        return form
    
    def get_context_data(self, **kwargs):
        context = super(CreateFuturesView, self).get_context_data(**kwargs)
        
        if 'product_id' in kwargs:
            product_id = kwargs.pop('product_id')
            product = models.Product.objects.get(id=product_id)
        else:
            product = models.Product.objects.get(id=self.request.resolver_match.kwargs['product_id'])
        context['product'] = product
        
        if hasattr(self, '_form'):
            form = self._form
        elif 'form' in context:
            form = context['form']
        else:
            form = self.get_form()
        context['form'] = form
        
        return context
    
    def post(self, request, product_id):
        # Product
        product = models.Product.objects.get(id=product_id)
        
        # Form
        form = self.form_class(
            request.POST,
            property_name_list = [(str(p.id), p.name) for p in product.property_name_list.order_by('name')]
        )
        
        if form.is_valid():
            # Goods
            goods = models.Goods.objects.create(
                user_id = request.user.user.id,
                product_id = product_id,
                # photo = ...
            )
            
            # inc relevant in product obj.
            product.relevant += 1
            product.save()
            
            cleaned_data = form.clean()
            
            # Properties
            for property_name in product.property_name_list.all():
                val = cleaned_data[str(property_name.id)]
                property = models.Property.objects.create(
                    name = property_name,
                    value = val,
                )
                goods.property_list.add(property)
            
            # Futures
            futures = models.Futures.objects.create(
                goods = goods,
                price = form.cleaned_data['price'],
                route = form.cleaned_data['route'],
                date_from = form.cleaned_data['date_from'],
                date_till = form.cleaned_data['date_till'] or form.cleaned_data['date_from'],
                time_from = form.cleaned_data['time_from'],
                time_till = form.cleaned_data['time_till'],
                address = form.cleaned_data['address'],
                # Weekdays
                mon = form.cleaned_data['mon'],
                tue = form.cleaned_data['tue'],
                wed = form.cleaned_data['wed'],
                thu = form.cleaned_data['thu'],
                fri = form.cleaned_data['fri'],
                sat = form.cleaned_data['sat'],
                sun = form.cleaned_data['sun'],
            )
            
            return redirect(self.redirect_url.format(futures.id))
        else:
            self._form = form
            return super(CreateFuturesView, self).get(request, product_id)

# Futures view (anti-futures list, deal-status-contacts)
class FuturesView(TemplateView):
    template_name = 'futures_view.html'
    
    def get_context_data(self, **kwargs):
        context = super(FuturesView, self).get_context_data(**kwargs)
        futures = models.Futures.objects.get(id=self.request.resolver_match.kwargs['id'])
        context['object'] = futures
        return context
    
    def get(self, request, id):
        # Check user
        if models.Futures.objects.get(id=id).goods.user.id != request.user.user.id:
            return redirect('/error/')
        # ...
        return super().get(request, id)

# Select mirror-futures
class FuturesSelectMirrorView(View):
    redirect_url = '/deal_view/{}/'
    
    def get(self, request, id, mirror_id):
        futures_q = models.Futures.objects.select_for_update().filter(
            id__in = (id, mirror_id),
            status='free'
        )
        if futures_q.count() != 2:
            return redirect('/error/')
        
        futures_q.update(status='busy')
        futures = models.Futures.objects.get(id=id)
        mirror_futures = models.Futures.objects.get(id=mirror_id)
        
        if futures.is_actual() and mirror_futures.is_actual(): pass
        else:
            models.Futures.objects.filter(id__in=(id, mirror_id)).update(status='free')
            return redirect('/error/')
        
        # Check user
        if (
            futures.goods.user.user != request.user
            #or futures.status != 'free'
            #or mirror_futures.status != 'free'
        ):
            models.Futures.objects.filter(id__in=(id, mirror_id)).update(status='free')
            return redirect('/error/')
        # ...
        
        deal = models.Deal.objects.create(
            buyer_futures = futures if futures.route == 'buy' else mirror_futures,
            seller_futures = futures if futures.route == 'sell' else mirror_futures,
            price = mirror_futures.price,
            date_from = max(futures.date_from, mirror_futures.date_from),
            date_till = min(futures.date_till, mirror_futures.date_till),
            time_from = max(futures.time_from, mirror_futures.time_from),
            time_till = min(futures.time_till, mirror_futures.time_till),
            # weekdays
            mon = futures.mon and mirror_futures.mon,
            tue = futures.tue and mirror_futures.tue,
            wed = futures.wed and mirror_futures.wed,
            thu = futures.thu and mirror_futures.thu,
            fri = futures.fri and mirror_futures.fri,
            sat = futures.sat and mirror_futures.sat,
            sun = futures.sun and mirror_futures.sun,
            # ...
            address = mirror_futures.address if (
                mirror_futures.address < futures.address
            ) else futures.address,
            status = 'wait',
        )
        
        #futures.status = 'busy'
        #futures.save()
        #mirror_futures.status = 'busy'
        #mirror_futures.save()
        
        send_mail(
            'Ваш фьючерс превратился в сделку.',
            'ID фьючерса: {}, ID сделки {}'.format(
                mirror_futures.id,
                deal.id,
            ),
            'noreply@geotsint.ru',
            [mirror_futures.goods.user.user.email],
            fail_silently=False
        )
        
        return redirect(self.redirect_url.format(deal.id))

# Null futures
class NullView(View):
    redirect_url = '/futures_view/'
    
    def get(self, request, id):
        result = models.Futures.objects.select_for_update().filter(id=id, goods__user__user=request.user).update(status='canc')
        #futures = models.Futures.objects.get(id=id)
        
        # Check user
        #if futures.goods.user.user != request.user:
        if not result:
            #models.Futures.objects.filter(id=id).update(status='canc')
            return redirect('/error/')
        # ...
        
        #futures.status = 'canc'
        #futures.save()
        
        return redirect(self.redirect_url)

"""
# Submit deal
class SubmitDealView(View):
    redirect_url = '/deal_view/{}/'
    
    def get(self, request, id, mirror_id):
        # Need blocking !!!
        futures = models.Futures.objects.get(id=id)
        mirror_futures = models.Futures.objects.get(id=mirror_id)
        
        # Check user
        if futures.goods.user.user != request.user:
            return redirect('/error/')
        # ...
        
        deal = models.Deal.objects.create(
            buyer_futures = futures if futures.route == 'buy' else mirror_futures,
            seller_futures = futures if futures.route == 'sell' else mirror_futures,
            price = mirror_futures.price,
            date_from = mirror_futures.date_from,
            date_till = mirror_futures.date_till,
            time = mirror_futures.time_from, # need !!!
            status = 'wait',
        )
        
        futures.status = 'busy'
        futures.save()
        mirror_futures.status = 'busy'
        mirror_futures.save()
        
        return redirect(self.redirect_url.format(deal.id))
"""

# Ok deal
class OkDealView(View):
    redirect_url = '/deal_view/'
    
    def get(self, request, id):
        deal = models.Deal.objects.get(id=id)
        
        self_futures = deal.get_self_futures(request.user)
        
        # Check user
        if self_futures.goods.user.user != request.user:
            return redirect('/error/')
        # ...
        
        self_futures.status = 'done'
        self_futures.save()
        
        if (
            deal.buyer_futures.status == 'canc'
            or deal.seller_futures.status == 'canc'
        ):
            deal.status = 'canc'
            deal.save()
        elif (
            deal.buyer_futures.status == 'done'
            and deal.seller_futures.status == 'done'
        ):
            deal.status = 'done'
            deal.save()
        
        return redirect(self.redirect_url)

# Cancel deal
class CancelDealView(View):
    redirect_url = '/deal_view/'
    
    def get(self, request, id):
        deal = models.Deal.objects.get(id=id)
        
        self_futures = deal.get_self_futures(request.user)
        
        # Check user
        if self_futures.goods.user.user != request.user:
            return redirect('/error/')
        # ...
        
        self_futures.status = 'canc'
        self_futures.save()
        
        if (
            deal.buyer_futures.status == 'canc'
            or deal.seller_futures.status == 'canc'
        ):
            deal.status = 'canc'
            deal.save()
        elif (
            deal.buyer_futures.status == 'done'
            and deal.seller_futures.status == 'done'
        ):
            deal.status = 'done'
            deal.save()
        
        return redirect(self.redirect_url)

# List of own futures
class FuturesListView(TemplateView):
    template_name = 'futures_list_view.html'
    
    def get_context_data(self):
        context = super().get_context_data()
        
        user = self.request.user.user
        futures_list = models.Futures.objects.filter(goods__user=user, status='free')
        context['futures_list'] = futures_list
        
        return context

# Deal list view
class DealListView(TemplateView):
    template_name = 'deal_list_view.html'
    
    def get_context_data(self):
        context = super().get_context_data()
        
        user = self.request.user.user
        deal_qs = models.Deal.objects.filter(
            Q(buyer_futures__goods__user = user)
            | Q(seller_futures__goods__user = user)
        ).filter(status='wait')
        
        deal_list = []
        for deal in deal_qs:
            deal._self_user = user
            deal_list.append(deal)
        context['deals'] = deal_list
        return context

class ErrorView(TemplateView):
    template_name = 'error.html'
    
    def get_context_data(self):
        context = super().get_context_data()
        
        error = self.request.GET.get('error')
        if error:
            context['error'] = error
        
        
        return context

# Edit "contacts" in User model
class EditContactsView(FormView):
    template_name = 'edit_contacts_view.html'
    form_class = forms.EditContactsForm
    
    def get_context_data(self):
        context = super().get_context_data()
        
        user = self.request.user.user
        init_form = dict(contacts=user.contacts)
        context['form'] = self.form_class(init_form)
        
        return context
    
    def post(self, request):
        user = self.request.user.user
        form = self.form_class(request.POST)
        if form.is_valid():
            user.contacts = form.cleaned_data['contacts']
            user.save()
        else:
            return redirect('/error/')
        
        return redirect('/')

# About
class AboutView(TemplateView):
    template_name = 'about.html'

class CurrentProductList(TemplateView):
    template_name = 'current_product_list.html'
    
    def get_context_data(self):
        context = super().get_context_data()
        
        futures_q_buy = models.Futures.objects.filter(status='free', date_till__gte=datetime.date.today(), route='buy')
        futures_q_sell = models.Futures.objects.filter(status='free', date_till__gte=datetime.date.today(), route='sell')
        goods_buy = models.Goods.objects.filter(id__in=futures_q_buy.values('goods').distinct())
        goods_sell = models.Goods.objects.filter(id__in=futures_q_sell.values('goods').distinct())
        products_buy = models.Product.objects.filter(id__in=goods_buy.values('product').distinct())
        products_sell = models.Product.objects.filter(id__in=goods_sell.values('product').distinct())
        context['products_buy'] = products_buy
        context['products_sell'] = products_sell
        return context