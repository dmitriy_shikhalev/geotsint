from django import forms
import django.forms
from django.contrib import auth
from . import models
import datetime

class LoginForm(forms.Form):
    login = forms.CharField(label='Логин', max_length=100)
    password = forms.CharField(label='Пароль', max_length=100, widget=forms.PasswordInput)
    
    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        
        login = cleaned_data.get('login')
        password = cleaned_data.get('password')
        
        user = auth.authenticate(username=login, password=password)
        if user is None:
            raise django.forms.ValidationError("Неверные логин/пароль")
        if not user.is_active:
            raise django.forms.ValidationError("Пользователь заблокирован")
        
        self.user = user
        
        return cleaned_data


class RegisterForm(forms.Form):
    login = forms.CharField(label='Ник', max_length=100)
    password = forms.CharField(label='Пароль', max_length=100, widget=forms.PasswordInput)
    password_confirm = forms.CharField(label='Пароль еще раз', max_length=100, widget=forms.PasswordInput)
    contacts = forms.CharField(label='Контакты', widget=forms.Textarea)
    email = forms.EmailField(label='Почта')
    
    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        
        if auth.models.User.objects.filter(
            username=cleaned_data.get('login')
        ).exists():
            raise django.forms.ValidationError("Логин уже существует")
        if auth.models.User.objects.filter(
            email=cleaned_data.get('email')
        ).exists():
            raise django.forms.ValidationError("E-mail уже существует")
        if cleaned_data.get('password') != cleaned_data.get('password_confirm'):
            raise django.forms.ValidationError('Пароли не совпадают')
        
        return cleaned_data


class SelectProductForm(forms.Form):
    words = forms.CharField(label='Слова для поиска', max_length=200, required=False)


class CreateProductForm(forms.Form):
    name = forms.CharField(label='Называние', max_length=1024, widget=forms.Textarea)
    property_name_list = forms.MultipleChoiceField(
        label = 'Названия свойств',
        widget = forms.CheckboxSelectMultiple,
        choices = zip(
            models.PropertyName.name_list(),
            models.PropertyName.name_list()
        ))


class CreateFuturesForm(forms.Form):
    price = forms.DecimalField(label='Цена', max_digits=models.PRICE_MAX_DIGITS)
    date_from = forms.CharField(label='Дата от')
    date_till = forms.CharField(label='Дата до', required=False)
    time_from = forms.TimeField(label='Время от', initial='00:00')
    time_till = forms.TimeField(label='Время до', initial='23:59')
    mon = forms.BooleanField(label='Пн', initial=True, required=False)
    tue = forms.BooleanField(label='Вт', initial=True, required=False)
    wed = forms.BooleanField(label='Ср', initial=True, required=False)
    thu = forms.BooleanField(label='Чт', initial=True, required=False)
    fri = forms.BooleanField(label='Пт', initial=True, required=False)
    sat = forms.BooleanField(label='Сб', initial=True, required=False)
    sun = forms.BooleanField(label='Вс', initial=True, required=False)
    address = forms.CharField(label='Место', max_length=300, help_text='введите адрес, разделяя позиции запятой')
    route = forms.ChoiceField(label='Направление', choices=models.ROUTES, widget=forms.RadioSelect)
    
    def __init__(self, *args, property_name_list, **kwargs):
        # Add property fields
        for property_name in property_name_list:
            self.base_fields[property_name[0]] = forms.CharField(label=property_name[1])
        
        ret = super(CreateFuturesForm, self).__init__(*args, **kwargs)
        
        self.fields['address'].widget.attrs['style'] = 'width:600px;'
    
    def clean_date_from(self):
        str = self.cleaned_data['date_from']
        return datetime.datetime.strptime(str, '%d.%m.%Y').date()
    
    def clean_date_till(self):
        str = self.cleaned_data['date_till']
        if str:
            return datetime.datetime.strptime(str, '%d.%m.%Y').date()
        # raise forms.ValidationError('Введите дату')
    
    def clean(self):
        data = super(CreateFuturesForm, self).clean()
        
        # try:
            # time_from = datetime.datetime.strptime(data['time_from'], '%H:%M')
            # if data['time_till']:
                # time_till = datetime.datetime.strptime(data['time_till'], '%H:%M')
            # else:
                # time_till = None
        # except Exception as err:
            # raise django.forms.ValidationError('{} {}'.format(data['time_from'], data['time_till']))
        
        return data


class EditContactsForm(forms.Form):
    contacts = forms.CharField(label='Контакты', widget=forms.Textarea)