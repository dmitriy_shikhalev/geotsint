from django.contrib import admin
from main import models


class UserAdmin(admin.ModelAdmin):
    list_display = ('user', 'status', 'reputation')

class WordAdmin(admin.ModelAdmin):
    pass

class PropertyNameAdmin(admin.ModelAdmin):
    pass

class ProductAdmin(admin.ModelAdmin):
    pass

class PropertyAdmin(admin.ModelAdmin):
    pass

class GoodsAdmin(admin.ModelAdmin):
    pass

class FuturesAdmin(admin.ModelAdmin):
    pass

class DealAdmin(admin.ModelAdmin):
    list_display = ('buyer_futures', 'seller_futures', 'status')

class LoginIPAdmin(admin.ModelAdmin):
    list_display = ('user', 'ip')


admin.site.register(models.User, UserAdmin)
admin.site.register(models.Word, WordAdmin)
admin.site.register(models.PropertyName, PropertyNameAdmin)
admin.site.register(models.Product, ProductAdmin)
admin.site.register(models.Property, PropertyAdmin)
admin.site.register(models.Goods, GoodsAdmin)
admin.site.register(models.Futures, FuturesAdmin)
admin.site.register(models.Deal, DealAdmin)
admin.site.register(models.LoginIP, LoginIPAdmin)
